import express from 'express';
import path from 'path';

import * as db from './db.js';
import * as helpers from './helpers.js';
import * as env from './config/env.js';

import bodyParser from 'body-parser';

var pug = require('pug').__express;

const app = express();

app.set('view engine', 'pug');
app.set('views', path.join(__dirname, '../views'));
app.use(bodyParser.json()); /* This line allows json data to come through - requires import of body-parser */;

app.use('/assets', express.static(path.join(__dirname, '../assets')));

app.post("/mark_as_sent", (req, res, next) => {
	db.markAsSent().then({

	}).catch( err => {

	});
});

app.get('*', (req, res, next) => {
	if (req.headers['accept'].indexOf('application/json') > -1){
		const query = req.query;
		db.getLatest(query).then(listings => {
			res.json(listings);
		}).catch(err => {
			res.status(404);
			res.render('404');
		});	
	} else {
		res.render('index', {helpers, env});	
	}
});

app.listen(8082, () => {
  console.log('App ready on port 8082!');
});