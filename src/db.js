import mongodb, {ObjectID} from 'mongodb';
import { dbPath } from './config/db.js';

const mongoPath = dbPath();
const MongoClient = mongodb.MongoClient;

const LISTING_COLLECTION = "listings"; 

const dbConnect = () => {
	return new Promise((resolve, reject) => {
		MongoClient.connect(mongoPath)
		.then(client => {
			resolve(client);
		}).catch(err => {
			reject(err);
		});
	});
}

export const getLatest = query => {
	return new Promise((resolve, reject) => {
			const skip = parseInt(query.skip); //mongo requires parseToInt
			dbConnect().then(client => {
				const parsedQuery = parseQuery(query);
				const fullQuery = Object.assign(parsedQuery, {listingStatus: true});
				let db = client.db();
				db.collection(LISTING_COLLECTION).find(fullQuery).sort({_id: -1}).skip(skip).limit(30).toArray((err, r) => {
					if (err) reject(err);
					resolve(r);
					client.close();
				})
			}).catch( err => {
				reject(err);
			});
		}
	);
}


export const markAsSent = query => {
	return newPromise((resolve, reject) => {
		dbConnect().then(client => {
			const db = client.db();
			db.collection(LISTING_COLLECTION).update({_id: ObjectID}, {$set: {emailSet: true}})
		}).catch( err => {
			reject(err);
		});
	})
}

const parseQuery = query => {

	delete query.skip;

	if (query.hasOwnProperty('email')){
		return query;
	}

	const METROPOLITAN_REGION = ["Metropolitana", "metropolitana", "Metropolitana de Santiago", "region_metropolitana"];

	if (query.hasOwnProperty("region")){
		if (query.region === "met"){
			query.region = { $in: METROPOLITAN_REGION }
		}
	}

	const COMMERCIAL_PURPOSE = ["Comercial e industrial", "local", "industrial", "Propiedadindustrial", "Negocio/Patentes/Derechosdellave"];

	if (query.hasOwnProperty("propertyType")){
		if (query.propertyType === "commercial"){
			query.propertyType = { $in: COMMERCIAL_PURPOSE }
		}
	}


	return query;
}