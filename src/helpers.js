import numeral from 'numeral';

export const getFullUrl = listing => {
	const platform = listing.platform;

	const ECON_BASE_URL = "http://www.economicos.cl";
	if (platform === "ECON"){
		return ECON_BASE_URL + listing.url;
	}

	const YAPO_BASE_URL = "https://www.yapo.cl";
	if (platform === "YAPO"){
		return YAPO_BASE_URL + listing.url;
	}

	const PORTAL_BASE_URL = "https://www.portalinmobiliario.com";
	if (platform === "PORTAL"){
		return PORTAL_BASE_URL + listing.url;
	}

	return null;
}

export const priceFormatter = price => {
	return numeral(price).format('$0,0');
}

export const dateFromObjectId = objectId => {
	const timestamp = objectId.toString().substring(0, 8);
	const date = new Date( parseInt (timestamp, 16) * 1000);
	return date;
}

export const isOwner = description => {
	return (description.toLowerCase().indexOf("dueño") > -1);
}