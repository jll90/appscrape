$(document).ready(function(){

	$("#load-more-btn").on('click', loadMore);

	var $modal = $(".email-modal");
	var $emailInput = $("form input#email");
	var $nameInput = $("form input#name");
	var $sendBtn = $("form button");
	var $closeModalBtn =$("form #close-modal");

	

	$sendBtn.on('click', sendEmail);
	$closeModalBtn.on('click', closeModal);
	
	function loadMore(){
		var listingsCount = $(".listing").length;
		var querySearch = window.location.search;

		var queryPath = window.location.pathname + querySearch;

		if (querySearch.length > 0){
			queryPath += "&skip=" + listingsCount;
		} else {
			queryPath += "?skip=" + listingsCount;
		}

		$.ajax({
			url: queryPath,
			dataType: "json"
		}).done(function(data){
			if (data.length === 0){
				alert("No existen publicaciones con estos parametros");	
			}
			addListingsToTable(data);
			rebindEvents();
		}).fail(function(){
			alert("Ha habido un error conectando al servidor");
		});
	}

	function rebindEvents(){
		$(".listing .email").on('click', function(){
			var email = $(this).html();
			var name = $(this).siblings(".contact-name").html();
			showModal();
			setEmail(email);
			setName(name);
		});
	}

	function showModal(){
		$modal.show();
	}

	function setEmail(email){
		$emailInput.val(email);
	}

	function setName(name){
		$nameInput.val(name)
	}

	function closeModal(){
		$modal.hide();
	}

	function sendEmail(){
		$.ajax({
			url: "http://server.penthouse.solutions/mailer/get_to_know_us",
			data: parseFormValues(),
			type: "POST",
			dataType: "json"
		}).done(function(){
			closeModal();
			alert("Correo enviado correctamente");
		}).fail(function(){
			alert("Error. No se pudo enviar el correo.");
		});
	}

	function markAsSent(){
		$.ajax({

		})
	}

	function parseFormValues(){
		var email = $emailInput.val();
		var name = $nameInput.val();
		var gender = $("form input[name='gender']:checked").val();

		return {
			mailer: {
				to_email: email,
				client_name: name,
				gender: gender
			}
		}
	}

	function addListingsToTable(listings){
		var length = listings.length;
		var listingsHtml = "";
		for (var i=0; i < length; i ++){
			listingsHtml += printListing(listings[i]);
		}
		$("table tbody").append(listingsHtml);
	}

	function printListing(listing){
		var l = listing;
		var html = "";

		var IS_OWNER = isOwner(l.description);
		var IS_BROKER = isBroker(l.email, l.contactName, l.phoneNumber);

		if ( IS_OWNER && !IS_BROKER){
			html += "<tr class='listing table-success'>";	
		} else if (IS_BROKER){
				html += "<tr class='listing table-info'>";
			} else {
				html += "<tr class='listing'>";
			}

		html += 	"<th>" + l.platform + "</br>";
		html += 		l.code + "</br>";
		html += 		"<a target='_blank' href='"+ getFullUrl(l) +"'>Ver</a>";
		html +=		"</th>";
		html += 	"<td>" + dateFromObjectId(l._id) + "</td>";
		html += 	"<td>"
		html +=			"<div><strong>" + l.propertyType + "</strong></div>";
		if (l.bedrooms !== undefined && l.bedrooms !== null){
			html +=			"<div>Dormitorios: " + l.bedrooms + "</div>";	
		}
		
		if (l.bathrooms !== undefined){
			html +=			"<div>Baños: " + l.bathrooms + "</div>";			
		}

		if (l.parkingSpots !== undefined){
			html +=			"<div>Estacionamientos: " + l.parkingSpots + "</div>";
		}
		
		if (l.storage !== undefined){
			html +=			"<div>Bodegas: " + l.storage + "</div>";	
		}

		html += 	"</td>";


		html +=		"<td>" + l.county + ", " + l.region + "</td>";

		html +=		"<td>" + priceFormatter(l.price) + "</td>";

		html +=		"<td>" + l.description + "</td>";

		html +=		"<td>";
		
		if (l.contactName !== undefined){
			html +=			"<div class='contact-name'>" + l.contactName + "</div>";
		}

		if (l.email !== undefined){
			html +=			"<div class='email' style='text-decoration: underline; cursor: pointer'>" + l.email + "</div>";
		}

		if (l.phoneNumber !== undefined){
			html +=			"<div>" + l.phoneNumber + "</div>";
		}
		html +=		"</td>";

		html += "</tr>";

		return html;
	}

	function dateFromObjectId (objectId) {
		const timestamp = objectId.toString().substring(0, 8);
		const date = new Date( parseInt (timestamp, 16) * 1000);
		return date;
	}

	function priceFormatter(price){
		return numeral(price).format('$0,0');
	}

	function getFullUrl (listing) {
		var platform = listing.platform;

		var ECON_BASE_URL = "http://www.economicos.cl";
		if (platform === "ECON"){
			return ECON_BASE_URL + listing.url;
		}

		var YAPO_BASE_URL = "https://www.yapo.cl";
		if (platform === "YAPO"){
			return YAPO_BASE_URL + listing.url;
		}

		var PORTAL_BASE_URL = "https://www.portalinmobiliario.com";
		if (platform === "PORTAL"){
			return PORTAL_BASE_URL + listing.url;
		}

		return null;
	}

	function isOwner (description) {
		if (description !== undefined && description !== null){
			return (description.toLowerCase().indexOf("dueño") > -1);
		}
		return false;
	}

	function isBroker (email, contactName, phoneNumber){

		var isBrokerRegex = /remax|propiedad|corredor|corretaje|arriendo|inmobiliaria|asociado|abogado|amoblado|venta|inmobiliario|raices|fullhaus|casa|inmueble|arquitecto|apart|hotel|administracion|procarvallo|asesoria|conygomez|gerencia|gestion|info|prop|inversiones|property|homie|home|homy|raeb/gi

		var emailMatches = email ? email.match(isBrokerRegex) : null;
		var contactNameMatches = contactName ? contactName.match(isBrokerRegex) : null;
		var phoneNumberMatches = phoneNumber ? phoneNumber.replace(/<\/?[^>]+(>|$)/g, "").match(isBrokerRegex) : null;

		var contactInfoMatch = (emailMatches !== null || contactNameMatches !== null || phoneNumberMatches !== null);

		var emailBlackList = [
			"hildainesgalleguillos@gmail.com",
			"jxgonza@gmail.com",
			"monica.garrido@vtr.net",
			"kliendo.0212@gmail.com",
			"pbascunanv7515@gmail.com",
			"egomez160@gmail.com",
			"carmenluz.latorre@gmail.com",
			"jgpaiss@gmail.com",
			"angela.delaluz.777@gmail.com"
		];

		return contactInfoMatch || (emailBlackList.indexOf(email) > 1);		
	}



	loadMore();

});