'use strict';

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _db = require('./db.js');

var db = _interopRequireWildcard(_db);

var _helpers = require('./helpers.js');

var helpers = _interopRequireWildcard(_helpers);

var _env = require('./config/env.js');

var env = _interopRequireWildcard(_env);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var pug = require('pug').__express;

var app = (0, _express2.default)();

app.set('view engine', 'pug');
app.set('views', _path2.default.join(__dirname, '../views'));
app.use(_bodyParser2.default.json()); /* This line allows json data to come through - requires import of body-parser */;

app.use('/assets', _express2.default.static(_path2.default.join(__dirname, '../assets')));

app.get('*', function (req, res, next) {
	if (req.headers['accept'].indexOf('application/json') > -1) {
		var query = req.query;
		db.getLatest(query).then(function (listings) {
			res.json(listings);
		}).catch(function (err) {
			console.log(err);
			res.status(404);
			res.render('404');
		});
	} else {
		res.render('index', { helpers: helpers, env: env });
	}
});

app.listen(8082, function () {
	console.log('App ready on port 8082!');
});