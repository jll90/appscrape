'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.getLatest = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _mongodb = require('mongodb');

var _mongodb2 = _interopRequireDefault(_mongodb);

var _db = require('./config/db.js');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var mongoPath = (0, _db.dbPath)();
var MongoClient = _mongodb2.default.MongoClient;

var LISTING_COLLECTION = "listings";

var dbConnect = function dbConnect() {
	return new Promise(function (resolve, reject) {
		MongoClient.connect(mongoPath).then(function (client) {
			resolve(client);
		}).catch(function (err) {
			reject(err);
		});
	});
};

var getLatest = exports.getLatest = function getLatest(query) {
	return new Promise(function (resolve, reject) {
		var skip = parseInt(query.skip); //mongo requires parseToInt
		dbConnect().then(function (client) {
			var parsedQuery = parseQuery(query);
			var fullQuery = _extends(parsedQuery, { listingStatus: true });
			var db = client.db();
			db.collection(LISTING_COLLECTION).find(fullQuery).sort({ _id: -1 }).skip(skip).limit(30).toArray(function (err, r) {
				if (err) reject(err);
				resolve(r);
				client.close();
			});
		}).catch(function (err) {
			reject(err);
		});
	});
};

var parseQuery = function parseQuery(query) {

	delete query.skip;

	var METROPOLITAN_REGION = ["Metropolitana", "metropolitana", "Metropolitana de Santiago", "region_metropolitana"];

	if (query.hasOwnProperty("region")) {
		if (query.region === "met") {
			query.region = { $in: METROPOLITAN_REGION };
		}
	}

	var COMMERCIAL_PURPOSE = ["Comercial e industrial", "local", "industrial", "Propiedadindustrial", "Negocio/Patentes/Derechosdellave"];

	if (query.hasOwnProperty("propertyType")) {
		if (query.propertyType === "commercial") {
			query.propertyType = { $in: COMMERCIAL_PURPOSE };
		}
	}

	return query;
};